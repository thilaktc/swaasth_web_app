(function() {
    'use strict';

	angular.module('swaasth.mprofile')
        .controller('FamilyMemberDetailsController', ['$scope', '$rootScope', 'MedProfileService', '$stateParams','$state', 'ProfileService', function($scope, $rootScope, MedProfileService, $stateParams, $state, ProfileService ) {
            console.log("FamilyMemberDetailsController");     
 			
            $scope.selectedTab = "personal";
            $scope.switchedTab = 2;

 			var param = $stateParams.params;
            console.log('param for new or old', param);

            $scope.getUserDetail = function () {
                $rootScope.showLoader();
                ProfileService.getUserDetails(param).then(function(response) {
                    $rootScope.hideLoader();
                    console.log('profileController[next] : : ', response);
                    $scope.userDetail = response;
                    $scope.currentUser = response;
                    $scope.setCurrentUser(response);
                    var currentUser = response;

                    console.log('$scope.user : : ', $scope.userDetail);

                    if (currentUser) {
                        for (var key in $scope.userDetail) {
                            if (key === 'dateOfBirth' && $scope.userDetail[key]) {
                                $scope.userDetail[key] = new Date(currentUser[key]);
                                if ($scope.userDetail[key] == 'Invalid Date') {
                                    $scope.userDetail[key] = '';
                                }
                            }
                            else {
                                $scope.userDetail[key] = currentUser[key];
                            }
                        }
                    }

                }).catch(function(error) {
                    $rootScope.hideLoader();
                });
            }

            $scope.editPersonnalInfo = function (type) {

            	console.log('type::', type);
            	if (type != 'emergencyContact') {
            		$state.go($scope.profileScreen[type], { params:'edit'});
            	} else {
            		$state.go('emergencyContact', { params:'edit'});
            	}
            };

            $scope.goToMedicalInfo = function(){
                $state.go('medicalinfo', { params: param });
            };

            $scope.showDashboard = function() {
                $state.go('dashboard', { params: param });
            }

	}]);

})();