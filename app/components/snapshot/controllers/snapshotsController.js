(function() {
    'use strict';

	angular.module('swaasth.snapshots')
        .controller('SnapshotsController', ['$scope', '$rootScope', 'MedProfileService', 'ProfileService', '$stateParams', '$state', function($scope, $rootScope, MedProfileService, ProfileService, $stateParams, $state ) {
            console.log("SnapshotsController");      
            
            $scope.selectedTab = "record";
            $scope.switchedTab = 1;

            var param = $stateParams.params;
            console.log('SnapshotsController param::', param);
            console.log('SnapshotsController state::', $state);

            $scope.getAge = function(dob){
                var dob = new Date(dob);
                var year = dob.getFullYear(), month=dob.getMonth(), date = dob.getDate();
                console.log(year, month, date);
                var age = new Date().getFullYear() - year;
                var mdif = new Date().getMonth() - month + 1;//0=jan   
                
                if(mdif < 0) {
                    --age;
                } else if(mdif == 0) {
                    var ddif = new Date().getDate() - date;
                    
                    if(ddif < 0) {
                        --age;
                    }
                }

                if(age == 0){
                    // age = moment().week()/52;
                    age = mdif/12;
                }
                console.log(age);
                return age;
            };

            $scope.getAllAssesmentDetails = function () {

            	$rootScope.showLoader();
            	var reqObj = {
            		"userId" : param
            	};
            	MedProfileService.getAllAssesmentDetails(reqObj).then(function(res){
                    $rootScope.hideLoader();
                    $scope.finalAssesmentData = res.completeAssesmentInfo;
                    console.log('get profile medical info-->', $scope.finalAssesmentData);
                    $scope.userAge = parseInt($scope.getAge($scope.finalAssesmentData.userDetails.dateOfBirth));
                    $scope.profileImage= $scope.finalAssesmentData.userDetails.profileImage;
                    $scope.gender = $scope.finalAssesmentData.userDetails.gender;
                }).catch(function(error){
                	$rootScope.hideLoader();
                    console.log('error while getting the list view', error);
                });
            };


           
	}]);

})();