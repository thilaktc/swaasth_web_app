var homeModule = angular.module("swaasth.allergies", []);

homeModule.config(['$stateProvider', '$urlRouterProvider', 'AllergiesRoutes', function($stateProvider, $urlRouterProvider, AllergiesRoutes) {
    $stateProvider
    
        .state(AllergiesRoutes.userAllergyView.name,{

            url: AllergiesRoutes.userAllergyView.url,
            templateUrl: 'components/allergies/partials/userAllergiesList.html'

        })

        .state(AllergiesRoutes.alleryDetailedView.name,{

            url: AllergiesRoutes.alleryDetailedView.url,
            templateUrl: 'components/allergies/partials/alleryDetailedView.html'

        })

        .state(AllergiesRoutes.allergies.name,{

            url: AllergiesRoutes.allergies.url,
            templateUrl: 'components/allergies/partials/allergieslist.html'

        })
        .state(AllergiesRoutes.allergiesTriggers.name,{
            url: AllergiesRoutes.allergiesTriggers.url,
            templateUrl: 'components/allergies/partials/allergiesTriggers.html'
        })
        
        .state(AllergiesRoutes.allergiesSymptoms.name,{
            url: AllergiesRoutes.allergiesSymptoms.url,
            templateUrl: 'components/allergies/partials/allergiesSymptoms.html'
        })

        .state(AllergiesRoutes.allergieAddDescription.name,{
            url: AllergiesRoutes.allergieAddDescription.url,
            templateUrl: 'components/allergies/partials/allergieAddDescription.html'
        })

}]);