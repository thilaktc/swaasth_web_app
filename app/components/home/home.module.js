var homeModule = angular.module("swaasth.home", []);

homeModule.config(['$stateProvider', '$urlRouterProvider', 'HomeRoutes', function($stateProvider, $urlRouterProvider, HomeRoutes) {
    $stateProvider
        .state(HomeRoutes.home.name,{

            url: HomeRoutes.home.url,
            templateUrl: 'components/home/partials/home.html'

        })

        .state(HomeRoutes.recommendations.name,{

            url: HomeRoutes.recommendations.url,
            templateUrl: 'components/home/partials/recommendationsListView.html'

        })

        .state(HomeRoutes.recommendationDetail.name,{

            url: HomeRoutes.recommendationDetail.url,
            templateUrl: 'components/home/partials/recommendationDetailView.html'

        })

        .state(HomeRoutes.aboutSwasth.name,{

            url: HomeRoutes.aboutSwasth.url,
            templateUrl: 'components/sidebar/aboutSwasth.html'

        })

        .state(HomeRoutes.termSwasth.name,{

            url: HomeRoutes.termSwasth.url,
            templateUrl: 'components/sidebar/termsCondition.html'

        })

        .state(HomeRoutes.privacyPolicy.name,{

            url: HomeRoutes.privacyPolicy.url,
            templateUrl: 'components/sidebar/privacyPolicy.html'

        })

        .state(HomeRoutes.contactUs.name,{

            url: HomeRoutes.contactUs.url,
            templateUrl: 'components/sidebar/contactUs.html'

        })

}]);