/*
 *
 *  Record Controller
 *
*/

(function() {
    'use strict';

    angular.module('swaasth.record')
        .controller('recordController', ['$scope', '$rootScope', '$stateParams', '$state', 'ProfileService', function($scope, $rootScope, $stateParams, $state, ProfileService) {

            console.log("recordCtrlCalled");

            $scope.selectedTab = "record";
            $scope.switchedTab = 1;

            $scope.getFamilyMember = function () {
            	$rootScope.showLoader();
				ProfileService.getFamilyMemberList().then(function(res){
                    $rootScope.hideLoader();
                    $scope.personalInfoDetailsList = res.result;
                    console.log('get profile medical info-->', $scope.personalInfoDetailsList);
                    console.log('get current usere',  $scope.currentUser);
                }).catch(function(error){
                	$rootScope.hideLoader();
                    console.log('error while getting the list view', error);
                });

			};
			$scope.getFamilyMember();


            $scope.showSnapshots = function (list) {
                $state.go('snapShotView', {params: list.user.id});
            }

    }]);
})();
           