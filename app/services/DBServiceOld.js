//(function() {
//
//    'use strict';
//
//    var DBService = function($q,$cordovaSQLite) {
//        var dbService = {};
//        var _db;
//        var _currentUser;
//
//
//        dbService.initDB = function () {
//            console.log("Before initDB");
//            _db = $cordovaSQLite.openDB({ name: "swaasth.db",location:'default' });
//            $cordovaSQLite.execute(_db,'CREATE TABLE IF NOT EXISTS UserDetails (id INTEGER PRIMARY KEY, firstName TEXT,middleName TEXT,lastName TEXT,dateOfBirth DATE,gender TEXT,profileImage TEXT,signature TEXT, bloodGroup TEXT,' +
//                'contactNumber VARCHAR(15),address1 TEXT,address2 TEXT,city TEXT,state TEXT,pincode NUMERIC,alternateContactNumber NUMERIC,emergencyContactNumber VARCHAR(15),emergencyContactPerson VARCHAR(15),emailId TEXT').then(function(res){
//
//                _db.closeDb();
//            });
//
//            console.log("After initDB");
//        };
//
//        dbService.getUserDetails  = function() {
//            _db = $cordovaSQLite.openDB({ name: "swaasth.db",location:'default' });
//
//            $cordovaSQLite.execute(_db,"SELECT * FROM UserDetails").then(function(result){
//
//                if(result.rows.length > 0) {
//
//                    console.log("rows=",result.rows);
//                }
//                _db.closeDb();
//            },function(error){ console.log("Error=",error.message)});
//
//        }
//
//        //dbService.getUserDetails = function(currentUser) {
//        //    var query = "SELECT * FROM UserDetails WHERE id="+currentUser.id;
//        //    $cordovaSQLite.execute(_db,query,[]).then(function(res) {
//        //
//        //        console.log("ResPonse all data=",res);
//        //    });
//        //
//        //};
//
//        dbService.insertUserDetails  = function(currentUser) {
//            console.log("Before insert details");
//            _db = $cordovaSQLite.openDB({ name: "swaasth.db",location:'default' });
//            $cordovaSQLite.execute(_db,"SELECT * FROM UserDetails WHERE id="+currentUser.id).then(function(result){
//
//                if(result.rows.length > 0) {
//                    var query = "UPDATE UserDetails SET id="+currentUser.id+", gender="+currentUser.gender+", firstName="+currentUser.firstName+"," +
//                        ",middleName= "+currentUser.middleName +", lastName="+currentUser.lastName+", dateOfBirth="+currentUser.dateOfBirth+", profileImage="+currentUser.profileImage+",address1="+currentUser.address1+",address2="+currentUser.address2+",city="+currentUser.city+",state="+currentUser.state+",pincode="+currentUser.pinCode+"" +
//                        ",alternateContactNumber="+currentUser.alternateContactNumber+",emergencyContactNumber="+currentUser.emergencyContactNumber+",emergencyContactPerson="+currentUser.emergencyContactPerson+",emailId="+currentUser.emailId;
//                    $cordovaSQLite.execute(_db,query,[]).then(function(res){
//                        console.log("updated successfully",res);
//                    },function(err){
//                        console.log("error while updating",err)
//                    });
//
//                } else {
//
//                    var query = "INSERT INTO UserDetails (id, firstName,middleName,lastName,dateOfBirth,gender,profileImage,signature, bloodGroup,"+
//                        +"contactNumber,address1,address2,city,state,pincode,alternateContactNumber,emergencyContactNumber,emergencyContactPerson,emailId) values(currentUser.id,currentUser.firstName,currentUser.middleName,currentUser.lastName,currentUser.dateOfBirth, currentUser.gender,currentUser.profileImage,currentUser.signature,currentUser.bloodGroup,currentUser.address1,currentUser.address2,currentUser.city,currentUser.state,currentUser.pinCode,currentUser.alternateContactNumber,currentUser.emergencyContactNumber,currentUser.emergencyContactPerson,currentUser.emailId)";
//                    $cordovaSQLite.execute(_db,query,[]).then(function(res){
//                        console.log("insertId: " + res);
//                    },function(err){
//                        console.log("error while inserting");
//                    });
//
//                }
//            },function(error){ console.log("Error=",error.message)});
//
//            console.log("After insert details");
//        }
//
//        return dbService;
//
//    };
//
//    DBService.$inject = [
//        '$q',
//        '$cordovaSQLite'
//    ];
//
//    angular
//        .module('swaasth')
//        .factory('DBService', DBService);
//})();