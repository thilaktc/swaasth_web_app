(function() {
    'use strict';

    angular.module('swaasth.home')
        .controller('recommendationController', ['$scope', '$rootScope', 'HomeService', '$stateParams', '$state', function($scope, $rootScope, HomeService, $stateParams, $state ) {
            console.log("recommendationController");

            //$state.go('recommendationsList');
            //$scope.isSocialUser = $scope.currentUser.isSocialUser || false;
            $scope.notificationList = [];
            $scope.recommendationExpand = false;
            /*if ($scope.isSocialUser){
                $scope.set_or_change_pass_url = "resetPassword";
                $scope.set_or_change_pass_text = "Set Password";
            }else{
                $scope.set_or_change_pass_url = "changePassword";
                $scope.set_or_change_pass_text = "Change Password";
            }*/
            
            $scope.getNotification = function() {
                var auth = {
                    "authToken" : localStorage.getItem('ngStorage-authToken')
                }

                $rootScope.showLoader();
                HomeService.getNotifications(auth).then( function(respone) {
                    console.info("notification :::-->>", respone.notifications);
                    $rootScope.hideLoader();
                    if(respone.statusCode == 200){
                        $scope.notificationList = respone.notifications.reverse();

                        /*setTimeout(function(){

                            for(var i = 0; i< $scope.notificationList.length; i++) {

                                document.getElementById('rec'+i).innerHTML = $scope.notificationList[i].notification;

                            }

                        },100);*/

                    } else {

                    }
                }).catch(function() {
                    $rootScope.hideLoader();
                });
            };

            $scope.showDetails = function (list) {
                $state.go('recommendationDetail', {params: list.id});
            }

	}]);

})();