/**
 * @description
 * 
 *
 * @author
 * Koushik
 */
(function() {

    'use strict';

    angular.module('swaasth.allergies')
        .controller('allergiesAddController', ['$scope', '$state', '$stateParams', '$rootScope', 'AllergyService', function($scope, $state, $stateParams, $rootScope, AllergyService) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;
            
        	$scope.treatMentFlag = true;
			$scope.testConductedFlag = true;
            $scope.isShownNotes = false;

            $scope.allergyName = $stateParams.allergyName;
            $scope.triggerBy = $stateParams.triggerBy;
            $scope.symtomsList = $stateParams.symtomsList.split(',');
            $scope.maxDate = new Date().toISOString().substring(0,10);
            $scope.minDate = new Date($scope.user.dateOfBirth).toISOString().substring(0,10);
            
            console.info("symptoms List :: -->", $scope.symtomsList);
            $scope.carousel = {};
            $scope.carousel.index = +$stateParams.index || 0;



        	$scope.toggleHeight = function($event) {
        		var target = $event.target.id;
        		$('#'+target).css('height','100%');
        	};

        	$scope.addTreatment = function() {
        		$scope.treatMentFlag = false;
        	};

			$scope.addTestConducted = function() {
				$scope.testConductedFlag = false;
			};

			$scope.closeTreatment = function() {
				$scope.treatMentFlag = true;
			};

			$scope.closeTest = function() {
				$scope.testConductedFlag = true;
			};

            $scope.showNotesPage = function() {
                $scope.isShownNotes = true;
            };

            var index = $stateParams.index, allergyData={};
            if(index){
                var details = AllergyService.getAllergiesForUser();
                allergyData = details[index];
            }
            
            $scope.allergy = {
                allergy: $scope.allergyName || '',
                triggeredBy: $scope.triggerBy || '',
                symptoms: $scope.symtomsList || '',
                treatment: allergyData.treatment || '',
                testConducted: allergyData.testConducted || '',
                measurementFrom: allergyData.fromDate || new Date(),
                measuredTo: allergyData.toDate || new Date(),
                note: allergyData.notes || ''
            };

            $scope.saveAllergyDetail = function(data){

                if ($scope.isShownNotes) {
                    $scope.isShownNotes = false;
                    return true;
                }

                $rootScope.showLoader();
/*Adding parameters to Diagnostic */
                var requestParam = {
                    allergy: $scope.allergyName,
                    triggeredBy: $scope.triggerBy,
                    symptoms: $scope.symtomsList,
                    treatment: $scope.allergy.treatment,
                    testConducted: $scope.allergy.testConducted,
                    notes: $scope.allergy.note,
                    fromDate: $scope.allergy.measurementFrom,
                    toDate: $scope.allergy.measuredTo,
                };

                if(index) {
                    requestParam['id'] = $stateParams.allergyId;
                }

                var reqObj = {};
                reqObj['authToken'] = localStorage.getItem('ngStorage-authToken');
                reqObj['allergy'] = requestParam;
                reqObj.userId = localStorage.getItem('mProfileUserId');
                
                if(index){
                    reqObj['type'] = "update";
                    reqObj['index'] = index;
                }

                AllergyService.CRUDAllergyDetails(reqObj).then(function(response) {
                    console.info('successfullly saved : : ', response);
                    $rootScope.hideLoader();
                    
                    if(index){
                        $state.go('userAllergyView', { params:  response.result.allergies});
                    } else {
                        $state.go('alleryDetailedView', { params:  $scope.allergyName, index: response.result.allergies.length-1});
                    }

                }).catch(function(error) {
                    console.warn('error while saving : : ', error);
                });
            };

    }]);
})();
