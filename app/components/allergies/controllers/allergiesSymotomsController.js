/**
 * @description
 * 
 *
 * @author
 * Koushik
 */
(function() {

    'use strict';

    angular.module('swaasth.allergies')
        .controller('allergiesSymotomsController', ['$scope', '$state', 'AllergyService', '$stateParams', '$rootScope', function($scope, $state, AllergyService, $stateParams, $rootScope) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;
            
        	$scope.triggerList = AllergyService.getStoredTiggersList();

        	$scope.symptomsList = [];
            $scope.allergyName = $stateParams.allergyName;
            $scope.triggerBy = $stateParams.triggerBy;
            $scope.allergyId = $stateParams.allergyId;

        	$scope.getSymptomsList = function() {
                $rootScope.showLoader();
                var user = {
                    authToken: localStorage.getItem('ngStorage-authToken'),
                }
                AllergyService.getSymptomsList(user).then(function(res) {
                    $rootScope.hideLoader();
        			$scope.symptomsList = res.symptoms;
        		}).catch(function(error) {
        			console.info('Failed to load Symptoms List ::', error);
        		});
        	};

            $scope.folder = {};
            $scope.symptomList = [];
            $scope.addNewSymptoms = function() {
                $(".in").hide();
                $("#addNewSymptoms").modal('hide');
                $scope.symptomList.push($scope.newSymptoms);
                $scope.goToAddDescription();
            };

            $scope.goToAddDescription = function() {
                angular.forEach($scope.folder, function(key, value){
                    if (key) $scope.symptomList.push(value);
                });

                console.info("symptoms selected :::-->",  $scope.symptomList );

                $state.go('allergieAddDescription', { allergyId: $scope.allergyId, allergyName: $scope.allergyName, triggerBy: $scope.triggerBy, symtomsList: $scope.symptomList});
            };

    }]);
})();
