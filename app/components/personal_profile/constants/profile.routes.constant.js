/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.profile').constant('ProfileRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/
        profile: {
            name: 'profile',
            url: '/profile/:params'
        },

        introduction: {
            name: 'profile.intro',
            url: '/introduction/:params',
        },

        personal: {
            name: 'profile.intro.personal',
            url: '/personaltab'
        },

        medical: {
            name: 'profile.intro.medical',
            url: '/medicaltab'
        },

        /*dashboard: {
            name: 'profile.intro.dashboard',
            url: '/dashboardtab'
        },*/

        names: {
            name: 'names',
            url: '/names/:params'
        },

        dob: {
            name: 'dob',
            url: '/dob/:params'
        },

        gender: {
            name: 'gender',
            url: '/gender/:params'
        },

        bloodGroup: {
            name: 'bloodGroup',
            url: '/bloodGroup/:params'
        },

        mobile: {
            name: 'mobile',
            url: '/mobile/:params'
        },

        emergencyContact: {
            name: 'emergencyContact',
            url: '/emergency_contact/:params'
        },
        
        address: {
            name: 'address',
            url: '/address/:params'
        },

        familyMember: {
            name: 'familyMember',
            url: '/familyMember'
        },

        familyMemberDetail: {
            name: 'familyMemberDetail',
            url: '/familyMemberDetail/:params'
        }
        
    });
})();
