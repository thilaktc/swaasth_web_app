(function() {
    'use strict';

    angular.module('swaasth.healthinsurance')
        .controller('healthInsuranceListController', ['$scope', '$rootScope', 'InsuranceService', '$stateParams', '$state', 'ProfileService', '$filter', function($scope, $rootScope, InsuranceService, $stateParams, $state, ProfileService, $filter) {

            $scope.selectedTab = "record";
            $scope.switchedTab = 1;

            if($state.current.name == 'healthInsuranceListView'){
                $('#indexCloud').addClass('cloud-light');
            }else{
                $('#indexCloud').removeClass('cloud-light');
            }
            
            console.log("healthInsuranceListController Called"); 
            $scope.headerText = 'Insurance';
            var param = $stateParams.params;
            $scope.insuranceDetailsList = {};
            $scope.isLoad = true;
        /* Call Service */   
        	$scope.addInsurance = function () {
                console.log('add insurance');
                $state.go('healthInsurance', { params:  ""});
            };

            $scope.showInsuranceDetail = function($index) {
                console.log('Show insurance');
                $state.go('healthInsuranceDetailView', { params:  JSON.stringify($scope.insuranceDetailsList[$index])});
            }
            $scope.getInsuranceDetails = function() {
                $rootScope.showLoader();
                var insuranceAuth = {
                    "authToken": localStorage.getItem('ngStorage-authToken')
                };

                InsuranceService.getUserInsurance(insuranceAuth).then(function(response) {
                    
                    $scope.insuranceDetailsList = response.Insurance;
                    //$scope.insuranceDetails.expiryDate = $filter('date')($scope.insuranceDetails.expiryDate, "yyyy-MM-dd");
                    $rootScope.hideLoader();
                    $scope.isLoad = false;
                }).catch(function(error) {
                    $rootScope.hideLoader();
                    console.log('error while getting the insurance list', error);
                });            
            }

            $scope.goBack = function() {
                $state.go('record');
            }
            
    }]);

})();
