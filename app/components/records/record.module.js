var recordModule = angular.module("swaasth.record", []);

recordModule.config(['$stateProvider', '$urlRouterProvider', 'RecordRoutes', function($stateProvider, $urlRouterProvider, RecordRoutes) {
    $stateProvider
        .state(RecordRoutes.record.name,{

            url: RecordRoutes.record.url,
            templateUrl: 'components/records/partials/record.html'

        })

        .state(RecordRoutes.recordMemberList.name,{

        	url: RecordRoutes.recordMemberList.url,
        	templateUrl: 'components/records/partials/recordMemberList.html'
        })

}]);