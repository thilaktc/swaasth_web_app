/*global angular */
(function() {
    'use strict';


    /**
     * @description This is useful for comparing the two fields.
     *              In-short, It checks whether the two fields are same or not.
     *
     * @author Tushar
     */

    angular.module('swaasth.auth').directive('compareTo', function() {
        return {
            scope: {
                targetModel: '=compareTo'
            },
            require: 'ngModel',
            link: function postLink(scope, element, attrs, ngModel) {

                var compare = function() {

                    var sourceValue = element.val();
                    var targetValue = scope.targetModel;

                    if (targetValue !== null) {
                        return sourceValue === targetValue;
                    }

                    return false;
                };

                scope.$watch(compare, function(newValue) {
                    ngModel.$setValidity('errorCompareTo', newValue);
                });
            }
        };
    });
})();
