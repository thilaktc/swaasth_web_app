/**
 * @description 
 *displayin the diagnostic Details
 *              
 * @author Koushik
 */
(function() {

    'use strict';

    var DiagnosticService = function($q, $http, $localStorage, BASE_URL, API) {

        var diagnostic = {};
        var diagnosticsList = [];
        var diagnosticsTestList = [];
        var diagnosticsUnitList = [];
        var diagnosticsMethodList = [];
        var diagnosticListOfmedicalDetails = [];


        diagnostic.getDiagnosticsMetaData = function(user) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.getDiagnosticsMetaData, user)
                .then(function(res) {
                    console.info('DiagnosticService [getDiagnosticsMetaData] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);

                        diagnosticsList = res.data.result.diagnosticsList;
                        diagnosticsTestList = res.data.result.diagnosticsTestList;
                        diagnosticsUnitList = res.data.result.diagnosticsUnitList;
                        diagnosticsMethodList = res.data.result.diagnosticsMethodList;
                        
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        diagnostic.getStoredTestList = function(){
            return diagnosticsTestList;
        };

        diagnostic.getStoredUnitList = function(){
            return diagnosticsUnitList;
        };

        diagnostic.getStoredMethodList = function(){
            return diagnosticsMethodList;
        };

        diagnostic.getProfileMedicalInfo = function (category) {
            var deffered = $q.defer();
            $http
                .post(BASE_URL.url + API.getMedicalDetails, category)
                .then(function (res) {
                    
                    if(res.data.statusCode === 200){
                        deffered.resolve(res.data);
                        diagnosticListOfmedicalDetails = res.data;
                    } else if(res.data.statusCode === 201){
                        deffered.resolve(res.data);
                    } else {
                        deffered.reject(res.data);
                    }
                }).catch(function(error) {
                    deffered.reject(error);
                });

            return deffered.promise;
        };

        diagnostic.getStoredDiagnosticDetails = function() {
            return diagnosticListOfmedicalDetails;
        };

        diagnostic.deleteM = function(data) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.deleteMedicalDetails, data)
                .then(function(res) {
                    console.info('medProfileService [delete] : : ', res);
                    if (res.data.statusCode === 200) {
                        console.info('medProfileService [delete] : : ');
                        deferred.resolve(res.data);
                    } else if (res.data.statusCode === 210) {
                        console.info('medProfileService [delete] : : Not allowed');
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        diagnostic.save = function(data) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.addMedicalDetails, data)
                .then(function(res) {
                    console.info('medProfileService [save] : : ', res);
                    if (res.data.statusCode === 200) {
                        console.info('medProfileService [save] : : ');
                        deferred.resolve(res.data);
                    } else if (res.data.statusCode === 210) {
                        console.info('medProfileService [save] : : Not allowed');
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        return diagnostic;
    };

    DiagnosticService.$inject = [
        '$q',
        '$http',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.diagnostic')
        .factory('DiagnosticService', DiagnosticService);
})();
