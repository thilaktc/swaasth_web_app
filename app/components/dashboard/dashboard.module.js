var dashboard = angular.module("swaasth.dashboard", []);

dashboard.config(['$stateProvider', '$urlRouterProvider', 'AuthRoutes', function($stateProvider, $urlRouterProvider, AuthRoutes) {

    $stateProvider
        .state('dashboard', {

            url: '/dashboard:params',
            templateUrl: 'components/dashboard/partials/dashboard.html'

        });

}]);