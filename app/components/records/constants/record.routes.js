/*globals angular */
(function() {
    'use strict';

    /**
     * @description 
     * Contains the Routes Constants, which are available across the swaasth.profile module.
     * 
     * @author 
     * Tushar
     */
    angular.module('swaasth.record').constant('RecordRoutes', {

        /**********************************************************************
                    PROFILE ROUTES
        **********************************************************************/
        record: {
            name: 'record',
            url: '/record'
        },

        recordMemberList: {
            name: 'recordMemberList',
            url: '/recordMemberList'
        }


    });
})();