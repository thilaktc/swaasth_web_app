(function() {
    'use strict';

    angular.module('swaasth.mprofile')
        .controller('medicalListController', ['$scope', '$rootScope', 'MedProfileService', '$stateParams', '$state', 'ProfileService', function($scope, $rootScope, MedProfileService, $stateParams, $state, ProfileService) {


            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;

            console.log("MedicalListCtrlCalled");
            $scope.carousel = {};
            $scope.carousel.index = +$stateParams.index || 0;
           
            var param = $stateParams.params;
            if (param == 'heartRate') {
                param = 'heartbeat';
            }

            console.log("Getting users parameters::-->", param);
            console.log('profile current user-->', $scope.currentUser);
            ProfileService.getUserDetails(localStorage.getItem('mProfileUserId')).then(function(response) {
                if(response){
                    $scope.currentUser = response;
                }
                console.log('currentUser',response);
            });

            $scope.medicalDetailsList = [];
            //$scope.headerText = MedProfileService.getheaderText(param);

            var paramObject = swaasthRequestObj[param];
            $scope.headerText = paramObject.default.header;
            $scope.subheaderText = paramObject.default.subheader;
            $scope.isLoad = true;

        /* Call Service */
            $scope.getListView = function (callback) {
                $rootScope.showLoader();

                var category = {
                    authToken: localStorage.getItem('ngStorage-authToken'),
                    param: param,
                    userId: localStorage.getItem('mProfileUserId')
                };
                MedProfileService.getProfileMedicalInfo(category).then(function(res){
                    $rootScope.hideLoader();
                    $scope.isLoad = false;
                    $scope.medicalDetailsList = res.generalInfo[param];
                    console.log('get profile medical info-->', $scope.medicalDetailsList);
                    console.log('get current usere',  $scope.currentUser);

                    if(callback){
                        callback($scope.medicalDetailsList);
                    }
                }).catch(function(error){
                    console.log('error while getting the list view', error);
                    $rootScope.hideLoader();
                });
            };

            
            $scope.generateDetailView = function() {
                // var details = MedProfileService.getStoredProfileDetails();
                $scope.getListView(function(data){
                    $scope.singleDetail = data;
                });
                // $scope.carousel.index = $stateParams.index;
            };

            $scope.deleteMedicalData = function() {
                console.log('carouselIndex',$scope.carousel.index);
                var data = $scope.singleDetail[$scope.carousel.index];
                $rootScope.showLoader();

                var reqObj = {};
                reqObj['authToken'] = localStorage.getItem('ngStorage-authToken');
                reqObj[param] = data;
                
                reqObj.userId = localStorage.getItem('mProfileUserId');

                console.log(reqObj);
                MedProfileService.deleteM(reqObj).then(function(response) {
                    console.info('successfullly saved : : ', response);
                    $rootScope.hideLoader();
                    $state.go('medicalListView', {params: $state.params.params});

                }).catch(function(error) {
                    console.warn('error while saving : : ', error);
                });

            };

            $scope.closeModal = function () {
                $("#modalview-login").kendoMobileModalView("close");
            };

            $scope.addMedicalInfo = function () {
                console.log('adscreen');
                $state.go('medicalProfileAdd', { params:  param});
            };

            $scope.editMedicalInfo = function () {
                console.log('edit screen');
                $state.go('medicalProfileAdd', { params:  param, index: $scope.carousel.index});
            };

            $scope.showMedicalDetail = function ($index) {
                $state.go('medicalDetailView', { params:  param, index: $index});
            }

            $scope.goBack = function() {
                if($rootScope.previousState == "medicalProfileAdd" && $rootScope.currentState == "medicalDetailView"){
                    $state.go('medicalListView', {params: $state.params.params});
                }else if($rootScope.currentState == "medicalListView"){
                    console.log($scope.currentUser.id);
                     $state.go('medicalinfo', { params: $scope.currentUser.id });
                }else{
                   navigator.app.backHistory();  
                }
            }

            
    }]);

})();

            