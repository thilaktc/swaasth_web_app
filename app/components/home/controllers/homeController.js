(function() {
    'use strict';

    angular.module('swaasth.home')
        .controller('homeController', ['$scope', '$rootScope', '$state', function($scope, $rootScope, $state ) {
            console.log("homeController");

            $scope.selectedTab = "home";
            $scope.switchedTab = 1;

            $('.rec-batch').remove();
            $state.go('home.recommendationsList');

	}]);

})();