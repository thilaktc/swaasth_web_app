/**
 * @description
 * 
 *
 * @author
 * Koushik
 */
(function() {

    'use strict';

    angular.module('swaasth.allergies')
        .controller('allergiesListController', ['$scope', '$state', 'AllergyService', '$rootScope', function($scope, $state, AllergyService, $rootScope) {

            $scope.selectedTab = "medical";
            $scope.switchedTab = 2;

        	$scope.allergiesList = [];
            $scope.newallery = "";
            $scope.triggerby = "";

        	$scope.getAllergies = function(callback) {
                $rootScope.showLoader();
                var user = {
                    authToken: localStorage.getItem('ngStorage-authToken'),
                }
                AllergyService.getAllergiessList(user).then( function(res) {
                    $rootScope.hideLoader();
        			$scope.allergiesList = res.allergies;
        		}).catch(function(error) {
        			console.log('error while getting the Allergies', error);
        		});

        		if(callback) {
        			callback($scope.allergiesList);
        		}
        	};

        	$scope.goToallergyView = function(id) {
                if( id == 9){
                    $("#addNewAllery").modal('show');
                } else {
            		$state.go('allergiesTriggers', { allergyId : id });
            		console.info("Allergy Id:::---> ", id);
                }
        	};

            $scope.addNewAllergy = function() {
                $(".in").hide();
                $("#addNewAllery").modal('hide');
                $state.go('allergiesSymptoms',{ allergyId: 9, allergyName: $scope.newallery, triggerBy: $scope.triggerby });
            };
    }]);
})();
