/**
 * @description
 * Manages the Profile of the user and allows user 
 * to updates its personal details
 * 
 * @author
 * Tushar
 */
(function() {
    'use strict';

    angular.module('swaasth.profile')
        .controller('ProfileController', ['$scope', '$rootScope', '$filter', '$state', 'ProfileRoutes', 'MedicalRoutes', 'ProfileService', 'AuthRoutes', '$stateParams', function($scope, $rootScope, $filter, $state, ProfileRoutes, MedicalRoutes, ProfileService, AuthRoutes, $stateParams) {

            var param = $stateParams.params;
            console.log('param for new or old', param);
            $scope.mediaOption = { show: false };

            $scope.progress = { profile: 0, medical: 0 };
            $scope.genderTxt = "";
            //$scope.user = {};
            $scope.backFooterIcon = false;
            $scope.screenNum = localStorage.getItem('screenNo');

            $('#indexCloud').addClass('cloud-light');

            $scope.selectedTab = "personal";
            $scope.switchedTab = 2;

            var date = new Date();
            var newDate = date.toISOString().substring(0, 10);
            newDate = new Date(newDate).getTime();
            newDate = new Date(newDate + 86400000 - 1);

            $scope.maxDate = newDate.toISOString().substring(0, 10);

            $scope.genderObj = { M: "Male", F: "Female", O: "OTHERS" };
            var currentUser = null;


            $scope.fetchAvatar = function(source) {


                console.info('Fetching Avatar..');
                ProfileService.browseImage(source).then(function(uploadResponse) {
                    // var response = JSON.parse(uploadResponse.response);
                    $scope.user.profileImage = uploadResponse;
                    console.info('Avatar Fetched..');
                    console.info(uploadResponse);
                    $scope.mediaOption.show = false;
                }).catch(function(error) {
                    console.warn('Error while fetching image ', error);
                    $scope.mediaOption.show = false;
                })

            };


            $scope.getUserDetail = function(paramId) {

                if (paramId != 'new') {

                    ProfileService.getUserDetails(paramId).then(function(response) {
                        console.log('profileController[next] : : ', response);
                        $scope.currentUser = response;
                        currentUser = response;

                        console.log('$scope.currentUser : : ', $scope.currentUser);

                        if (currentUser) {
                            for (var key in $scope.currentUser) {
                                if (key === 'dateOfBirth' && $scope.currentUser[key]) {
                                    $scope.currentUser[key] = new Date(currentUser[key]);
                                    if ($scope.currentUser[key] == 'Invalid Date') {
                                        $scope.currentUser[key] = '';
                                    }
                                }
                                else {
                                    $scope.currentUser[key] = currentUser[key];
                                }
                            }
                        }
                        $scope.checkGender();

                        $scope.goToUnfilledScreen();

                    }).catch(function(error) {

                    });
                } else {
                    //if it is new user
                    $scope.goToUnfilledScreen();
                }
            };

            $scope.isPhoneNoFocused = false;
            $scope.setPhoneNoFocused = function(status) {

                $scope.isPhoneNoFocused = status;

            };

            $scope.setDob = function() {
                var monthNames = [
                    "January", "February", "March",
                    "April", "May", "June", "July",
                    "August", "September", "October",
                    "November", "December"
                ];

                var date = new Date($scope.user.dateOfBirth);
                var day = date.getDate();
                var monthIndex = date.getMonth();
                var year = date.getFullYear();

                $scope.user.dateOfBirth = (day + ' ' + monthNames[monthIndex] + ' ' + year);

            }

            $scope.$watch('currentUser', function(newVal, oldVal) {
                var count = 0;
                console.log('newVal : : ', newVal);
                for (var key in newVal) {
                    var val = newVal[key];
                    if (val !== '' && val) {
                        count++;
                    }
                }
                // updateProgress(count);
            }, true);


            /**
             * @description
             * Updates the progress bar as user fills its details
             * 
             * @param  {number} filledCount [Count of number of fields user has filled]
             *
             * @author
             * Tushar
             */
            var updateProgress = function(filledCount) {
                $scope.progress.profile = (100 * filledCount) / Object.keys($scope.user).length;
                console.info('$scope.progress.profile :: ', $scope.progress.profile);
            };

            // $scope.progress.profile = ProfileService.getProfileProgress();

            $scope.isMale = $scope.isFemale = $scope.isOthers = false;

            $scope.checkGender = function() {
                if ($scope.currentUser && $scope.currentUser.gender) {
                    if ($scope.currentUser.gender == 'M') {
                        $scope.isMale = true;
                    } else if ($scope.currentUser.gender == 'F') {
                        $scope.isFemale = true;
                    } else {
                        $scope.isOthers = true;
                    }
                }
            }

            $scope.isClicked = function(gender) {

                switch (gender) {
                    case 1:
                        $scope.isFemale = true;
                        $scope.isMale = $scope.isOthers = false;
                        $scope.currentUser.gender = "F";
                        $scope.genderTxt = "Female";
                        break;
                    case 2:
                        $scope.isMale = true;
                        $scope.isFemale = $scope.isOthers = false;
                        $scope.currentUser.gender = "M";
                        $scope.genderTxt = "Male";
                        break;
                    case 3:
                        $scope.isOthers = true;
                        $scope.isMale = $scope.isFemale = false;
                        $scope.currentUser.gender = "O";
                        $scope.genderTxt = "OTHERS";
                        break;
                }

            };

            $scope.goToUnfilledScreen = function() {

                var screenNo = localStorage.getItem('screenNo');

                if (+screenNo) {
                    $state.go($scope.profileScreen[screenNo], { params: $scope.currentUser.id});
                } else {
                    $state.go($scope.profileScreen[0], { params: $scope.currentUser.id });
                    localStorage.setItem('screenNo', 1);
                }

                // $scope.screenNum = screenNo;

            };

            $scope.editDoneButton = function() {

            }

            $scope.showPersonalDetail = function() {
                //profile.intro.personal
                //$state.go('profile.intro.personal');
                $state.go('familyMemberDetail', { params: $scope.currentUser.id });

            };

            $scope.goToMedicalInfo = function() {
                $state.go('medicalinfo', { params: $scope.currentUser.id });
            };

            $scope.showDashboard = function() {
                $state.go('dashboard', { params: $scope.currentUser.id });
            };

            if (!$scope.user.emailId) {
                $scope.getUserDetail(param);
            } else if ((param == 'new') || (param != 'new' && !param)) {
                $scope.goToUnfilledScreen();
            }
            $scope.getUserDetail(param);

        }]);
})();
