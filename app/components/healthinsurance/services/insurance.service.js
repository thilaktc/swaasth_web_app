/**
 * @description 
 *Service for Health Insurance Details
 *              
 * @author Srikanth
 */
(function() {

    'use strict';

    var InsuranceService = function($q, $http, $localStorage, BASE_URL, API) {
        var insurance = {},
            insuranceProviders = [{name : 'Bajaj'}, {name : 'TATA AIG'}, {name : 'Max Bupa'}, {name : 'Star Health'}],
            policyTypes = ['Individual', 'Family', 'Corporate'],
            policyTerms = ['Days', 'Years'];

        var userInsuranceList = [];


        insurance.saveInsurance = function(insuranceDetails){
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.addInsurance, insuranceDetails)
                .then(function(res) {
                    console.info('InsuranceService [saveInsurance] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);                        
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        insurance.deleteInsurance = function(insuranceDetails){
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.deleteInsurance, insuranceDetails)
                .then(function(res) {
                    console.info('InsuranceService [saveInsurance] : : ', res);
                    if (res.data.statusCode === 200) {
                        deferred.resolve(res.data);                        
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        insurance.getUserInsurance = function(insuranceAuth){
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.fetchInsurance, insuranceAuth)
                .then(function(res) {
                    console.info('InsuranceService [saveInsurance] : : ', res);
                    if (res.data.statusCode === 200) {
                        userInsuranceList = res.Insurance || [];
                        deferred.resolve(res.data);                        
                    } else {
                        deferred.reject(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };
        
        insurance.getStoredInsuranceProviders = function(){
            return insuranceProviders;
        };

        
        insurance.getStoredPolicyTypes = function(){
            return policyTypes;
        };

        insurance.getStoredPolicyTerms = function(){
            return policyTerms;
        };            

        insurance.save = function(data) {
            var deferred = $q.defer();
            $http
                .post(BASE_URL.url + API.addInsuranceDetails, data)
                .then(function(res) {
                    console.info('InsuranceService [save] : : ', res);
                    if (res.data.statusCode === 200) {
                        console.info('InsuranceService [save] : : ');
                        deferred.resolve(res.data);
                    } else if (res.data.statusCode === 210) {
                        console.info('InsuranceService [save] : : Not allowed');
                        deferred.resolve(res.data);
                    }
                }).catch(function(error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        return insurance;
    };

    InsuranceService.$inject = [
        '$q',
        '$http',
        '$localStorage',
        'BASE_URL',
        'API'
    ];


    angular
        .module('swaasth.healthinsurance')
        .factory('InsuranceService', InsuranceService);
})();
